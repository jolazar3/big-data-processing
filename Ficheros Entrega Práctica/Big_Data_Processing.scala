import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, from_json, regexp_replace}
import org.apache.spark.sql.types.{IntegerType, StringType, StructType}

object Kafka_Scala_Practica {

  def main (args:Array[String]):Unit= {
    val spark = SparkSession.builder().appName("kafkajson").master("local[2]").getOrCreate()
    val df=spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers","localhost:9092")
      .option("subscribe","topicjson")
      .option("startingOffsets","earliest")
      .load()

    //castear los datos leídos en formato kafka, para que sean legibles y convertirlos en string

    val res=df.selectExpr("CAST(value AS STRING)")
    val schema = new StructType().add("id",StringType).add("first_name",StringType).add("last_name",StringType)
      .add("email",StringType).add("gender",StringType).add("ip_address",StringType)
    val persona= res.select(from_json(col("value"),schema).as("data"))
      .select("data.*")

    //Para poder utilizar el regexp_replace, hemos encontrado el método foldLeft que junto al withColumn
    //nos permite tratar todas las columnas y aplicarle el filtrado de las palabras. Sin el fold_left sólo
    //nos permitía ir una a una
    val Persona_tm = Seq("id","first_name","last_name","email","gender","ip_address")
      .foldLeft(persona) { (memoDF, colName) =>
      memoDF.withColumn(
        colName,regexp_replace(col(colName), "male|ani", "")
      )
    }
    print("Visualizamos los datos:")
    
    Persona_tm.writeStream
      .format("console")
      .outputMode("update")
      .start()
      .awaitTermination()
  }
}